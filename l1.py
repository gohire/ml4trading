import pandas as pd
import matplotlib.pyplot as plt


def test_run():
    start_date = '2017-03-06'
    end_date = '2018-03-02'
    dates = pd.date_range(start_date, end_date)
    total = pd.DataFrame(index=dates)
    stocks = ['AAPL', 'AMZN']
    for stock in stocks:
        tmp = pd.read_csv('data/{}.csv'.format(stock), index_col='Date', parse_dates=True,
                          usecols=['Date', 'Adj Close'], na_values=['nan'])
        tmp = tmp.rename(columns={'Adj Close': stock})
        total.fillna(method='ffill', inplace=True)
        total = total.join(tmp, how='inner')
        # total = total.dropna(subset=[stock])
    total = total / total.ix[0, :]
    total.plot()
    plt.show()


if __name__ == "__main__":
    test_run()
